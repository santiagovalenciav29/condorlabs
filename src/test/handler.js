const fetch = require('node-fetch');


async function search({ pageNumber, pageSize, from, to, region , sort}) {
    const dataCondorLabs = await fetch("https://hiring.condorlabs.io/api/countries/all")
    const dataJson = await dataCondorLabs.json();
// apartir de aqui se comienzan a hacer los filtros
    const population = dataJson.filter( p => p.population > from && p.population < to);
    const filterRegion = dataJson.filter( r => r.region == region );
    const populationAndRegion = population.filter( r => r.region == region );
    const sorted = (json,sort,pageNumber,pageSize) => {
        if(sort.name === 'asc' ){
            const listed = json.sort( (i,f) => {
                const countryI = i.name.toUpperCase();
                const countryF = f.name.toUpperCase();
                if(countryI > countryF) return 1;
                if(countryI < countryF) return -1;
            });
            const result = listed.slice((pageNumber-1)*pageSize, pageNumber*pageSize).map(country => country.name);
            return result;
        } else if( sort.name === 'desc'){
            const listed2 = json.sort( (i,f) => {
                const countryI = i.name.toUpperCase();
                const countryF = f.name.toUpperCase();
                if(countryI > countryF) return -1;
                if(countryI < countryF) return 1;
            });
            const result = listed2.slice((pageNumber-1)*pageSize, pageNumber*pageSize).map(country => country.name);
            return  result;
        }
    }
 
    if(to && from && !region){
        return population
    } else if ( region && !to && !from) {
        return sorted(filterRegion, sort,pageNumber,pageSize)
    } else if (!region && !to && !from){
        return sorted(dataJson, sort,pageNumber,pageSize)
    } else {
        return populationAndRegion
    }

}

// async function search (){
//  const result = await filters()
//  console.log(result)
// }

// search()

module.exports = {search}