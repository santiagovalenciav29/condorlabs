'use strict';
const express = require('express');
const handler = require('./handler');
// App
const app = express();
const router = express.Router();

app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use('/', [router]);


app.get('/test',  async (req, res) => {
    const data = await handler.search({
        from: req.query.from,
        to: req.query.to,
        region: req.query.region,
        sort: req.query.sort,
        pageNumber: req.query.pageNumber,
        pageSize: req.query.pageSize
    })
    // console.log( await handler.search({ pageNumber: 10, pageSize: 1, region: 'Asia' , sort: { name : "desc"}}))
    res.json(data);
});
// Constants
const PORT = 4000;
const HOST = '0.0.0.0';

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);
module.exports = app ;
